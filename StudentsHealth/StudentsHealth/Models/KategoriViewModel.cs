﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentsHealth.Models;

namespace StudentsHealth.Models
{
    public class KategoriViewModel
    {
        public Kategori Kategori { get; set; }
        public List<Kategori> AltKategoriler { get; set; }
        public List<Kategori> YanKategoriler { get; set; }
    }
}