﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentsHealth.Models
{
    public class DuyuruViewModel
    {
        public List<Duyuru>DuyuruBaslik { get; set; }
        public List<Duyuru> Duyuruİcerik { get; set; }
    }
}