﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudentsHealth.Models;
using System.IO;

namespace StudentsHealth.Controllers
{
    public class DuyuruController : Controller
    {
        public string UploadImage(HttpPostedFileBase image)
        {
            var imagePath = Path.Combine(Server.MapPath("~/Content/template/img/"), image.FileName);
            var imageUrl = Path.Combine("~/Content/template/img/", image.FileName);
            while (System.IO.File.Exists(imagePath))
            {
                Random rnd = new Random();
                string fileName = Path.GetFileNameWithoutExtension(image.FileName) + "-" + rnd.Next(0, 999) + Path.GetExtension(image.FileName);
                imagePath = Path.Combine(Server.MapPath("~/Content/template/img/"), fileName);
                imageUrl = Path.Combine("~/Content//template/img/", fileName);
            }
            image.SaveAs(imagePath);
            return imageUrl;
        }
        private StudentsHealthEntities db = new StudentsHealthEntities();

        // GET: Duyuru
        public ActionResult Index()
        {
            var duyuru = db.Duyuru.Include(d => d.Kategori).Include(d => d.Kategori1);
            ViewBag.DuyuruListe = db.Duyuru.ToList();
            return View(duyuru.ToList());
        }
        

        // GET: Duyuru/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.DuyuruListe = db.Duyuru.ToList();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyuru duyuru = db.Duyuru.Find(id);
            if (duyuru == null)
            {
                return HttpNotFound();
            }
            return View(duyuru);
        }
       

        // GET: Duyuru/Create
        public ActionResult Create()
        {

            ViewBag.DuyuruListe = db.Duyuru.ToList();
            ViewBag.Kategori = Kategoriler();
            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text");
            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text");
            return View();
        }
        public List<KategoriViewModel> Kategoriler()
        {
            var kategoriler = db.Kategori.Where(x => x.UstKategoriID == null).ToList();
            List<KategoriViewModel> kategoriList = new List<KategoriViewModel>();
            foreach (var item in kategoriler)
            {
                kategoriList.Add(new KategoriViewModel()
                {
                    Kategori = item,
                    AltKategoriler = db.Kategori.Where(x => x.UstKategoriID == item.KategoriID).ToList(),
                    YanKategoriler = db.Kategori.Where(y => y.UstKategoriID > 5).ToList()
                });
            }
            return kategoriList;
        }
        

        // POST: Duyuru/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DuyuruID,İmageUrl,Baslik,İcerik,KategoriID")] Duyuru duyuru ,HttpPostedFileBase image)
        {

            ViewBag.DuyuruListe = db.Duyuru.ToList();
            if (ModelState.IsValid)
            {
                string imageUrl = UploadImage(image);
                duyuru.İmageUrl = imageUrl;
                db.Duyuru.Add(duyuru);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text", duyuru.KategoriID);
            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text", duyuru.KategoriID);
            return View(duyuru);
        }

        // GET: Duyuru/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.DuyuruListe = db.Duyuru.ToList();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyuru duyuru = db.Duyuru.Find(id);
            if (duyuru == null)
            {
                return HttpNotFound();
            }
            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text", duyuru.KategoriID);
            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text", duyuru.KategoriID);
            return View(duyuru);
        }

        // POST: Duyuru/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DuyuruID,İmageUrl,Baslik,İcerik,KategoriID")] Duyuru duyuru,HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                string imageUrl = UploadImage(image);
                duyuru.İmageUrl = imageUrl;
                db.Entry(duyuru).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text", duyuru.KategoriID);
            ViewBag.KategoriID = new SelectList(db.Kategori, "KategoriID", "Text", duyuru.KategoriID);
            return View(duyuru);
        }

        // GET: Duyuru/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyuru duyuru = db.Duyuru.Find(id);
            if (duyuru == null)
            {
                return HttpNotFound();
            }
            return View(duyuru);
        }

        // POST: Duyuru/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Duyuru duyuru = db.Duyuru.Find(id);
            db.Duyuru.Remove(duyuru);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
