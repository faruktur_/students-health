﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentsHealth.Models;
using System.Net;

namespace StudentsHealth.Controllers
{
    public class HomeController : Controller
    {
        StudentsHealthEntities ent = new StudentsHealthEntities();
       
        public ActionResult Index()
        {
            ViewBag.Kategoriler = Kategoriler();
            ViewBag.Goster = ent.Duyuru.Take(4).ToList(); 
            
            return View();
        }

        public ActionResult Duyurular(int? id)
        {
            ViewBag.Kategoriler = Kategoriler();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var duyuru = ent.Duyuru.Where(x=>x.KategoriID ==id).ToList();
            return View(duyuru);
        }
        public ActionResult Uye()
        {
            
            return View();
        }

        public ActionResult Duyuru(int? id)
        {
            ViewBag.Yorumlar = ent.Yorum.Where(x => x.DuyuruID == id).ToList();
            ViewBag.DuyuruListe = ent.Duyuru.Take(4).ToList();
            ViewBag.Kategoriler = Kategoriler();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyuru duyuru = ent.Duyuru.Find(id);
            if (duyuru == null)
            {
                return HttpNotFound();
            }
            return View(duyuru);
        }

        public List<KategoriViewModel> Kategoriler()
        {
            var kategoriler = ent.Kategori.Where(x => x.UstKategoriID == null).ToList();
            List<KategoriViewModel> kategoriList = new List<KategoriViewModel>();
            foreach (var item in kategoriler)
            {
                kategoriList.Add(new KategoriViewModel()
                {
                    Kategori = item,
                    AltKategoriler = ent.Kategori.Where(x => x.UstKategoriID == item.KategoriID).ToList(),
                    YanKategoriler = ent.Kategori.Where(y=>y.UstKategoriID>5).ToList()
                });
            }
            return kategoriList;
        }
        

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public class AnaSayfaDTO
        {
            public List<Slider> slider { get; set; }
            public List<Duyuru> duyuru { get; set; }
            public List<Kategori> kategori { get; set; }
            public List<Yorum> yorum { get; set; }
            public List<Kullanici> kullanici { get; set; }

        }
    }
}